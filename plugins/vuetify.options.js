export default {
  theme: {
    dark: false,
    options: { customProperties: true },
    themes: {
      dark: {
        primary: '#33c',
        accent: '#33c',
        secondary: '#33c',
        info: '#33c',
        warning: '#33c',
        error: '#33c',
        success: '#33c',
      },
      light: {
        primary: '#181718',
        accent: '#3fd',
        secondary: '#fac482',
        info: '#3fd',
        warning: '#3fd',
        error: '#3fd',
        success: '#3fd',
      },
    },
  },
}
